# 概要
旅行者が利用する画像ブログサービスを作成しました。

heroku上(http://quiet-scrubland-93022.herokuapp.com/)

## 旅行者のメリット
 - 今まで行った旅行先をあとで確認することができる。
 - 旅行行ったことを自慢できる。
 - 他の人にいい観光地を教えることができる。

## 閲覧者のメリット
 - 各都道府県に旅行しに行く際、観光スポットを知ることができる。

# システム

## 使い方

* ダウンロード

    ```
        git clone https://ferretdayo@bitbucket.org/ferretdayo/cookpad.git
        cd cookpad
    ```

* 初期設定

    ```
        composer install            // 各種データのインストール
        cp .env.example .env        // 環境ファイルのコピー
        vim .env                    // 編集
        php artisan key:generate    // プロジェクトキーの生成
        php artisan migrate         // マイグレーションの実行
    ```

* 起動

    ```
        php artisan serve
    ```