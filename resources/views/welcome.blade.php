@extends('layouts.app')

@section('content')
<div class="jumbotron search-head">
    <search-article></search-article>
</div>
<div class="container">
    <div v-show="selected !== ''">
        <h3 class="selected-title">@{{ selected }} の記事</h3>
    </div>
    <div v-show="selected === ''">
        <h3 class="selected-title">すべての記事</h3>
    </div>
    <div class="row" v-show="articles.length > 0">
        <div class="col-md-6" v-for="article in articles" :key="article.id">
            <article class="thumbnail">
                <img class="header-image" v-bind:src='"storage/images/" + article.image_path' alt="header">
                <div class="caption">
                    <div class="caption-info">
                        <div class="caption-location">
                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                            @{{ article.prefecture }}
                        </div>
                        <div class="post-user">
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                            @{{ article.user.name }}さん
                        </div>
                    </div>
                    <a class="caption-link" v-bind:href="'article/' + article.id">
                        <h3 class="article-title">@{{ article.title }}</h3>
                        @{{ article.outline }}
                    </a>
                </div>
                <div class="footer">
                    @{{ article.created_at }}
                </div>
            </article>
        </div>
    </div>
    <div class="row" v-show="articles.length <= 0">
        <div class="col-md-6">
            記事はありません。
        </div>
    </div>
</div>
@endsection
