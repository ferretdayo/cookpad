@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            {{-- 指定の記事の表示 --}}
            @if(isset($article))
                <article class="article">
                    <h3 class="article-title">{{ $article->title }}</h3>
                    <div class="caption-info">
                        <div class="caption-location">
                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                            {{ $article->prefecture }}
                        </div>
                        <div class="post-user">
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                            {{ $article->user->name }}さん
                        </div>
                    </div>
                    @if(isset(Auth::user()->id) && Auth::user()->id === $article->user->id)
                        <div style="float: right; margin-bottom: 10px;">
                            <a class="btn btn-success" style="width: 80px" href="{{ url('article/' . $article->id . '/edit') }}">編集</a>
                        </div>
                    @endif
                    <img class="img-responsive" src="{{ asset('storage/images/' . $article->image_path) }}" alt="header">
                    <div class="article-content">
                        {!! $article->content !!}
                    </div>
                    <div class="footer">
                        {{ $article->created_at }}
                    </div>
                </article>
            @endif
        </div>
    </div>
</div>
@endsection
