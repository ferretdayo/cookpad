@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if(count($articles) > 0)
                @foreach($articles as $article)
                    <div class="thumbnail">
                        <img src="{{ asset('storage/images/' . $article->image_path) }}" alt="header">
                        <div class="caption">
                            <div class="caption-info">
                                <div class="caption-location">
                                    <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                    {{ $article->prefecture }}
                                </div>
                                <div class="post-user">
                                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                    {{ $article->user->name }}さん
                                </div>
                            </div>
                            <a class="caption-link" href="{{ url('article/' . $article->id) }}">
                                <h3 class="article-title">{{ $article->title }}</h3>
                                {{ $article->outline }}
                            </a>
                        </div>
                        <div class="footer">
                            {{ $article->created_at }}
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
@endsection
