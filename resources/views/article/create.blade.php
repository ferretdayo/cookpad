@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if(count($errors) > 0)
                @foreach($errors->all() as $error)
                    <div class="errors bg-danger">{{ $error }}</div>
                @endforeach
            @endif
            <h3>記事の投稿</h3>
            <form class="form-horizontal" action="{{ url('article') }}" method="post" enctype="multipart/form-data" accept-charset='utf-8'>
                {{ csrf_field() }}
                <article-form></article-form>
            </form>
        </div>
    </div>
</div>
@endsection
