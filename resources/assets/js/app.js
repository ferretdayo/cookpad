
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import axios from 'axios';
import bus from './bus';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('article-form', require('./components/ArticleForm.vue'));
Vue.component('article-edit-form', require('./components/ArticleEditForm.vue'));
Vue.component('search-article', require('./components/SearchArticle.vue'));
Vue.component('show-my-article', require('./components/ShowMyArticle.vue'));
Vue.component('example', require('./components/Example.vue'));

axios.defaults.headers.common['csrfToken'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

const app = new Vue({
    el: '#app',
    data: function() {
        return {
            selected: "",
            articles: [],
        }
    },
    created(){
        bus.$on('searchArticles', ([articles, selected]) => {
            this.articles = articles;
            this.selected = selected;
        })
    },
});
