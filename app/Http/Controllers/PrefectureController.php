<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;

class PrefectureController extends Controller
{
    public function __construct()
    {

    }

    /**
     * 都道府県ごとの記事情報を指定された件数ずつ返すAPI
     * @param $filter 検索したい都道府県
     * @return 数件の記事情報が入ったjson
     */
    public function index(Request $request)
    {
        if(isset($request->filter) && $request->filter !== "") {
            $articles = Article::with('user')
                        ->where('prefecture', $request->filter)
                        ->orderBy('created_at', 'desc')
                        ->paginate(8);
            return response()->json($articles);
        }
        $articles = Article::with('user')
                    ->orderBy('created_at', 'desc')
                    ->paginate(8);
        return response()->json($articles);
    }
}
