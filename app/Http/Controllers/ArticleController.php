<?php

namespace App\Http\Controllers;

use App\Article;
use JavaScript;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\UploadedFile;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
    }

    public function index(Request $request)
    {
        $articles = Article::where('user_id', Auth::id())
                        ->orderBy('created_at', 'desc')
                        ->paginate(8);
        return response()->json($articles);
    }

    public function update(Request $request, $id)
    {
        $article = Article::find($id);
        if ($article->updateValidate($request->all()) === false ) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($article->errors());
        }
        $article->title = $request->title;
        $article->prefecture = $request->prefecture;
        $article->outline = $this->makeOutline(e($request->article));
        $article->content = $this->markdown2html(e($request->article));
        $article->markdown = e($request->article);
        $article->save();
        return redirect('/article/' . $id)->with('success', '更新しました。');
    }

    public function create()
    {
        return view('article.create');
    }

    public function destory()
    {

    }

    /**
     * 記事の投稿
     */
    public function store(Request $request)
    {
        $article = new Article;
        if ($article->validate($request->all()) === false ) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($article->errors());
        }

        if ($request->hasFile('img') && $request->file('img')->isValid([])) {
            $filename = $request->img->store('public/images');
            $article->user_id = Auth::id();
            $article->title = $request->title;
            $article->prefecture = $request->prefecture;
            $article->image_path = basename($filename);
            $article->outline = $this->makeOutline(e($request->article));
            $article->content = $this->markdown2html(e($request->article));
            $article->markdown = e($request->article);
            $article->save();
            return redirect('/home')->with('success', '保存しました。');
        } else {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(['error' => 'something wrong.']);
        }
    }

    /**
     * 記事の内容から概要を作成する。
     * @param $markdown マークダウンの文字列
     * @return マークダウンから概要を作成した文字列
     */
    private function makeOutline($markdown)
    {
        $markdowns = mb_split("\r\n|\n|\r|#", $markdown);
        $markdowns = array_map('trim', $markdowns);
        $markdowns = array_filter($markdowns, 'strlen');
        $markdowns = array_values($markdowns);
        $outline = "";
        foreach($markdowns as $markdown) {
            $outline .= $markdown;
        }
        if(mb_strlen($outline) > 40) {
            $outline = mb_substr($outline, 0, 40, "UTF-8")."...";
        }
        return $outline;
    }
    /**
     * markdownをhtmlに変換する関数
     * @param $markdown string
     * @return $html string
     */
    private function markdown2html($markdown)
    {
        $markdowns = mb_split("\r\n|\n|\r", $markdown);
        $html = "";
        // ここでHTMLに変換する
        foreach($markdowns as $row) {
            $tmp = $this->headline($row);
            $html .= $this->bold($tmp);
            $html .= "<br>";
        }
        return $html;
    }

    /**
     * hタグに変換する関数
     * @param $row string
     * @return $html string
     */
    private function headline($row)
    {
        $row = trim($row);
        if(strpos($row, '#') === false) {
            return $row;
        } else {
            $h = 0;
            for($i = 0; $i < mb_strlen($row, "UTF-8"); $i++) {
                if(mb_substr($row, $i, 1, "UTF-8") === '#') {
                    $h++;
                } else {
                    break;
                }
            }
            if($h > 0) {
                $html = "<h" . $h . ">" . trim(mb_substr($row, $h, null, "UTF-8")) . "</h" . $h . ">";
                return $html;
            }
            return $row;
        }
    }

    /**
     * boldタグに変換する関数
     * @param $row string
     * @return $html string
     */
    private function bold($row)
    {
        $html = "";
        $boldHtml = "";
        $boldTextLength = 0;
        $boldcnt = 0;
        for($i = 0; $i < mb_strlen($row, "UTF-8"); $i++) {
            $currentChar = mb_substr($row, $i, 1, "UTF-8");
            if($currentChar === '*') {
                $boldcnt++;
            }
            if($boldcnt === 0) {
                $html .= $currentChar;
            } else if($boldcnt === 1) {
                if($currentChar === '*') {
                    continue;
                }
                $boldHtml .= $currentChar;
            } else if($boldcnt === 2) {
                $boldcnt = 0;
                $html .= "<b>" . trim($boldHtml) . "</b>";
                $boldHtml = '';
            }
        }
        return $html;
    }

    public function show($id)
    {
        $article = Article::find($id);
        if(!isset($article)) {
            abort(404);
        }
        return view('article.show')->with('article', $article);
    }

    public function edit($id)
    {
        $article = Article::find($id);
        if(!isset($article)) {
            abort(404);
        }
        JavaScript::put([
            'editArticle' => $article
        ]);
        return view('article.edit')->with('articleId', $id);;
    }
}
