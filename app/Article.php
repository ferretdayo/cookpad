<?php

namespace App;

use App\User;
use Validator;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    private $rules = [
        'title' => 'required|between:1,191',
        'prefecture' => 'required|between:1,191',
        'img' => 'required|file|image',
        'article' => 'required',
    ];
    private $updateRules = [
        'title' => 'required|between:1,191',
        'prefecture' => 'required|between:1,191',
        'article' => 'required',
    ];

    private $errors;

    //belongsTo
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function validate($data)
    {
        $validation = Validator::make($data, $this->rules);
        if ($validation->fails())
        {
            $this->errors = $validation->errors();
            return false;
        }
    
        return true;
    }

    public function updateValidate($data)
    {
        $validation = Validator::make($data, $this->updateRules);
        if ($validation->fails())
        {
            $this->errors = $validation->errors();
            return false;
        }
    
        return true;
    }

    public function errors()
    {
        return $this->errors;
    }
}
